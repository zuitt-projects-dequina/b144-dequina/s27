/*

HTTP Methods

	GET 	-->	retrieves
	POST	-->	creating
	PUT		-->	updating
	DELETE	-->	delete

______________
POSTMAN CLIENT
	- to improve the experience of sending request, inspecting responcses and debugging

	noddemon installation: (if nodemon is not functioning)
		npm install -g nodemon

*/

const http = require('http');

const port = 4000;

const server = http.createServer((req, res) => {

		// the HTTP Method of the incoming request can be accessed via the "method" property of the 'request' parameter
		// the method "GET" means that we will be retrieving or reading information
		if (req.url == '/items' && req.method == "GET") {
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('Data retrieved from data base.')
		}
		if (req.url == '/items' && req.method == "POST") {
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('Data to be sent to the database.')
		}
		if (req.url == '/updateItem' && req.method == "PUT") {
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('Update our resources.')
		}
		if (req.url == '/delete' && req.method == "DELETE") {
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('Delete our resources.')
		}

})

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`)